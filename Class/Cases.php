<?php
include_once ('Cartes.php');
class Cases
{
    public $nomLieu;
    public $prix;
    public $typeCase;
    public $carte;
    public $prixMaison;

    public function __construct(Array $data)
    {
        $this->hydrate($data);
    }

    public function hydrate(Array $data){
        $this->nomLieu = $data["nomLieu"];
        $this->prix = $data["prix"];
        $this->typeCase = $data["typeCase"];
        $this->carte = Cartes::class;
        $this->prixMaison = $data["prixMaison"];
    }

    public function caseNom ()
    {
        return $this->nomLieu;
    }

    public function setTypeCase($typeCase){
        return $this->typeCase = $typeCase;
    }

    public function getTypeCase(){
        return $this->typeCase;
    }

    public function setCarte(){
        $carte1 = ["nomLieu"=>$this->nomLieu,"infos"=>$this->prix,"typeCarte"=>$this->typeCase];
        $this->carte = new Cartes($carte1);
    }
    public function getCarte(){
        $currentCarte = $this->carte;
       return "</br>Nom du lieu : ".$currentCarte->Nomlieu()."</br> Infos : ".$currentCarte->InfosSurLieu()."€ </br>type de la carte : ".$currentCarte->typeCartes();
    }

    public function getPrix(){
        return $this->prix;
    }

    public function setPrix($prix){
        return $this->prix = $prix;
    }

    public function getPrixMaison(){
        return $this->prixMaison;
    }

    public function setPrixMaison($prix){
        return $this->prixMaison = $prix;
    }
}