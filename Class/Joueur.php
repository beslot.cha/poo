<?php
include_once('Cases.php');
include_once ('Cartes.php');
class Joueur
{
    public $numCase;
    public $nomJoueur;
    public $Case;
    public $argent;
    public function __construct(array $data)
    {
        $this->Case = Cases::class;
        $this->Carte = Cartes::class;
        $this->numCase = $data["numcase"];
        $this->argent = $data["argent"];
        $this->nomJoueur = $data["nomJoueur"];
    }


    public function getCaseNumJoueur(){
        return $this->numCase;
    }

    public function setNumCaseJoueur($num){
        $this->numCase = $num;
    }

    public function setCase($caseJoueur){
        $this->Case = $caseJoueur;
    }

    public function getCase(){
        return $this->Case;
    }

    public function getArgent(){
        return $this->argent;
    }

    public function setArgent($argent){
        $this->argent = $argent;
    }

    public function AfficherCaseJoueur(){
        $case = $this->getCase();
        echo("Le joueur est sur la case : ".$case->caseNom()."</br>");
    }

    public function AfficherCarte(){
        $case = $this->getCase();
        $case->setCarte();
        echo ("voici les infos de la carte :".$case->getCarte());
    }

    public function acheteVille(){
        $case = $this->getCase();
        $prixMaison = $case->getPrixMaison();
        $this->setArgent($this->getArgent()-$prixMaison);
        echo ("Vous avez acheter cette case au prix de " .$prixMaison."€</br>");
         echo ("Vous avez désormais " .$this->getArgent()."€</br>");
    }

    public function InfosJoueur(){
        echo("</br>Le joueur ".$this->nomJoueur." à ". $this->getArgent()."€</br></br>");
    }
}