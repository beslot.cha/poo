<?php

include_once("GroupeCases.php");
include_once ("Class/Cases.php");
include_once ("Class/Cartes.php");
include_once ('Class/lesDes.php');
include_once ('Class/Joueur.php');

/*CREATION DES CASES DU JEU*/
$case1=new Cases($case1);
$case2=new Cases($case2);
$case3=new Cases($case3);
$case4=new Cases($case4);
$case5=new Cases($case5);
$case6=new Cases($case6);
$case7=new Cases($case7);
$case8=new Cases($case8);
$case9=new Cases($case9);
$case10=new Cases($case10);
$case11=new Cases($case11);
$case12=new Cases($case12);

$cases = [$case1,$case2,$case3,$case4,$case5,$case6,$case7,$case8,$case9,$case10,$case11,$case12];
// creation joueur
$joueur1 = new Joueur(["numcase"=>0,"argent"=>200,"nomJoueur"=>"Albert Reporter"]);
$joueur1->InfosJoueur();

$joueur2 = new Joueur(["numcase"=>0,"argent"=>200,"nomJoueur"=>"Barack Afritt"]);
$joueur2 ->InfosJoueur();

$arrayJoueur = [$joueur1,$joueur2];
// nb face dé
$de = new lesDes(6);
for($j=0;$j<2;$j++){
    // LANCER DE Dé
    $nbOnDe =$de->random();
    $caseDuJoueur = $cases[$nbOnDe-1];

// on applique le  "deplacement" joueur
    $arrayJoueur[$j] ->setNumCaseJoueur($nbOnDe);
    $arrayJoueur[$j]->setCase($caseDuJoueur);
    $arrayJoueur[$j]->AfficherCaseJoueur();
    //verification de la case

    // Case parc gratuit
    if( $arrayJoueur[$j]->getCase()->caseNom() == "parc"){
        $arrayJoueur[$j]->setArgent( $arrayJoueur[$j]->getArgent()+ $arrayJoueur[$j]->getCase()->getPrix());
        echo("Vous êtes sur la case parc vous gagné : " .  $arrayJoueur[$j]->getCase()->getPrix() . "€ </br>");
        echo ("</br>Votre argent est désormais de : ". $arrayJoueur[$j]->getArgent()."</br>");
    }

    //double sur les dés
    if($de->getDe1() == $de->getDe2()) {
        $arrayJoueur[$j]->setArgent( $arrayJoueur[$j]->getArgent() - 50);
        echo("</br>Bien joué vous allez trop vite !! vous êtes en prison ,car vous avez fait un double</br>");
    }

    // case Prison
    if ($arrayJoueur[$j]->getCase()->caseNom() == "prison") {
        $arrayJoueur[$j]->setArgent( $arrayJoueur[$j]->getArgent() -  $arrayJoueur[$j]->getCase()->getPrix());
        echo("</br>Bien joué vous allez trop vite !! vous êtes en prison</br>");
        echo("vous êtes sortis de prison et ca vous a couté 50€ votre argent est désormais de : " .  $arrayJoueur[$j]->getArgent() . "€ </br>");
    }

    // case départ on gagne 200 euros
    if( $arrayJoueur[$j]->getCase()->caseNom() == "Case Départ"){
        $arrayJoueur[$j]->setArgent( $arrayJoueur[$j]->getArgent() +$arrayJoueur[$j]->getCase()->getPrix());
        echo("</br> vous êtes tomber sur la case départ et vous recevez : ". $arrayJoueur[$j]->getCase()->getPrix()."€</br> Vous avez désormais : ". $arrayJoueur[$j]->getArgent()."€ </br>");
    }

    // si c'est une ville
    if( $arrayJoueur[$j]->getCase()->getTypeCase() == "ville" && $de->getDe1() != $de->getDe2()){
        // achete la ville
        $arrayJoueur[$j]->acheteVille();
        //afficher carte correspondante
        $arrayJoueur[$j]->AfficherCarte();
    }
}


$joueur1->InfosJoueur();
$joueur2 ->InfosJoueur();



function ddd($array) {
    foreach($array as $a) {
        echo("<pre>");
        echo("<code>");
        var_dump($a);
        echo("</code>");
        echo("</pre>");
    }
    die();
}

function dd($a) {
    echo("<pre>");
    echo("<code>");
    var_dump($a);
    echo("</code>");
    echo("</pre>");
    die();
}

?>

<form action="index.php">
    <button>Relancer la partie</button>
</form>
