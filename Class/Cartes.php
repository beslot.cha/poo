<?php

class Cartes
{
    public $nomLieu;
    public $infos;
    public $typeCarte;

    public function __construct(Array $data)
    {
        $this->hydrate($data);
    }

    public function hydrate(Array $data){
        $this->nomLieu = $data["nomLieu"];
        $this->infos = $data["infos"];
        $this->typeCarte = $data["typeCarte"];
    }

    public function Nomlieu ()
    {
      return $this->nomLieu;
    }

    public function InfosSurLieu ()
    {
        return $this->infos;
    }

    public function typeCartes(){
        return $this->typeCarte;
    }
}