<?php


class lesDes
{
    public $face;
    public $de1;
    public $de2;

    public function __construct($data)
    {
        $this->face =$data;
    }

    public function  random(){
         $rand1 = rand(1,$this->face);
         $rand2 = rand(1,$this->face);
         echo ('vous avez lancé les dés et vous avez fait : '. $rand1." + ".$rand2."</br>");
         $this->setDe1($rand1);
         $this->setDe2($rand2);
         return $rand1 + $rand2;
    }

    public function setDe1($nbOnDe){
        return $this->de1 = $nbOnDe;
    }
    public function setDe2($nbOnDe){
        return $this->de2 = $nbOnDe;
    }

    public function getDe1(){
        return $this->de1;
    }

    public function getDe2(){
        return $this->de2;
    }
}