<?php
$case1 = [
    "nomLieu"=>"Paris",
    "prix"=>"250",
    "typeCase" => "ville",
    "prixMaison" =>"100",
];

$case2 = [
    "nomLieu"=>"luxembourg",
    "prix"=>"350",
    "typeCase" => "ville",
    "prixMaison" =>"200",
];
$case3 = [
    "nomLieu"=>"Berlin",
    "prix"=>"150",
    "typeCase" => "ville",
    "prixMaison" =>"150",
];
$case4 = [
    "nomLieu"=>"Case Départ",
    "prix"=>"200",
    "typeCase" => "départ",
    "prixMaison" =>"0",
];
$case5 = [
    "nomLieu"=>"parc",
    "prix"=>"100",
    "typeCase" => "parc",
    "prixMaison" =>"0",
];
$case6 = [
    "nomLieu"=>"Chance",
    "prix"=>"150",
    "typeCase" => "chance",
    "prixMaison" =>"0",
];
$case7 = [
    "nomLieu"=>"commu",
    "prix"=>"500",
    "typeCase" => "commu",
    "prixMaison" =>"0",
];
$case8 = [
    "nomLieu"=>"Gare de lyon",
    "prix"=>"500",
    "typeCase" => "gare",
    "prixMaison" =>"0",
];

$case9 = [
    "nomLieu"=>"Gare du Nord",
    "prix"=>"500",
    "typeCase" => "gare",
    "prixMaison" =>"0",
];

$case10 = [
    "nomLieu"=>"Gare monparnasse",
    "prix"=>"500",
    "typeCase" => "gare",
    "prixMaison" =>"0",
];

$case11 = [
    "nomLieu"=>"Gare 9-3/4",
    "prix"=>"500",
    "typeCase" => "gare",
    "prixMaison" =>"0",
];

$case12 = [
    "nomLieu"=>"prison",
    "prix"=>"50",
    "typeCase" => "prison",
    "prixMaison" =>"0",
];
